const Discord = require('discord.js')
const client = new Discord.Client()
const fox = require('./characters/fox.js')
const config = require('./config.json')

client.on('ready', () => {
  console.log('I am ready!')
})

client.on('message', (message) => {

  const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
  const command = args.shift().toLowerCase();

  if (!message.content.startsWith(config.prefix)) return;

  if (command === 'ping') {
    message.channel.send('pong!')
  }

  switch (command) {
    case 'fox':
      for (let i = 0 ; i < args.length ; i++) {
        message.channel.send(fox.foxData(args[i]));
      }
      break;
  }
})

client.login(config.token)