module.exports = {
    foxData: function(attack){
        switch (attack) {
            case 'jab1':
                return "**FOX - Jab 1**\n\n`Frames: 17`\n`Hit: 2-3`\n`IASA: 16`\n`Window second jab : 3-31`\n`Second punch starts: 6 (or later)`\n`Frame Advantage (FA): -10`\n\nhttps://zippy.gfycat.com/YearlyEdibleHarpseal.webm";
            break;
            case 'jab2':
                return "**FOX - Jab 2**\n\n`Frames: 20`\n`Hit: 3-4`\n`IASA: 19`\n`Window rapid jabs: 1-20`\n`Rapid jabs starts: 6 (or later)`\n`Frame Advantage (FA): -12`\n\nhttps://zippy.gfycat.com/FirmForsakenAngwantibo.webm";
            break;
            case 'jab3':
                return "**FOX - Jab 3 (Rapid jabs / Kickjabs)**\n\n`Frames: 36 (full cycle) / 6 (intermediary animation)`\n`Hits: 3-4 / 10-11 / 17-18 / 24-25 / 31-32`\n`Frame Advantage (FA): -8`\n\nhttps://zippy.gfycat.com/SpotlessSameArmadillo.webm"
            break;
            default:
                return;
            break;
        }

    }
}