# Commandes supported

!ping => pong!

## FOX FRAME DATA

Format : !fox [atq1] [atq2] [...]
Availables :

To show how to ask all the moves :
!fox commands (TBD)

To show primary properties :
!fox properties (TBD)

JABS
!fox jab1
!fox jab2
!fox jab3

TILTS (TBD)
!fox ftilt 
!fox highftilt
!fox lowftilt
!fox utilt
!fox dtilt
 
GROUND ATTACKS (TBD)
!fox dashattack
!fox fsmash
!fox usmash
!fox dsmash

AERIALS (TBD)
!fox nair
!fox upair
!fox fair
!fox dair
!fox bair

SPECIALS (TBD)
!fox upb
!fox sideb / !fox dash
!fox downb / !fox shine
!fox neutralb / !fox laser

OTHER (TBD)
!fox grab
!fox dashgrab
!fox airdodge
!fox spotdodge
!fox froll
!fox broll



